# KjsonParser

![Cerberus logo](https://assets.gitlab-static.net/uploads/-/system/project/avatar/14437099/kerby.png)

## Introduction

The KJson parser is a generic parser which uses relection to get the attributes (and its values) from an object instance, and it returns it transformed into a custom Json String.

This code was originally created to solve an issue in a legacy system where there were 3 different JSON parsers among different components of a Kerberised application.
We were not able to change the different JSON versions to just a single one, as they came from independent development teams, hence we had to create a workaround to prevent information loss.

## Version notes

* Version 1.0 notes:
	* The project to fix was quite old, so Spring and mockito were not allowed. Therefore is quite impossible to get 100% coverture.
* Version 1.1 notes:
	* Mockito was added and a hack was done to mock the reflection call, assuring the 100% coverture.
