package org.thalion.parser;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * 
 * Utils class to parse any class to custom KJson strings.
 * 
 * @author Angeles Broullon Lozano
 * @version 1.1
 *
 */
public class KJsonParser {

	private static final String OPEN_DELIMITER = "[";
	private static final String CLOSE_DELIMITER = "]";
	private static final String ELEMENT_CLASS_DELIMITER = ": ";
	private static final String ELEMENT_VALUE_DELIMITER = " = ";
	private static final String SPLITTER_DELIMITER = "; ";
	private static final String SPLITTER_COLLECTION_DELIMITER = ", ";
	private static final String NULL = null;

	/**
	 * Parses an Object which implements the KJsonElement interface to to a pseudo
	 * Json using an API. Includes the class delimiters.
	 * 
	 * @param o the Object to parse
	 * @throws KJsonParserException if it couldn't be parsed
	 */
	public String parseToKJsonString(Object o) throws KJsonParserException {
		if (o != null) {
			StringBuilder sb = new StringBuilder(OPEN_DELIMITER);
			sb.append(o.getClass().getSimpleName()).append(ELEMENT_CLASS_DELIMITER).append(toKJsonPlainString(o))
					.append(CLOSE_DELIMITER);
			return sb.toString();
		} else {
			throw new KJsonParserException("The object is null and can not be parsed");
		}
	}

	/**
	 * Splits the process in 5 main cases: custom classes, Collections, arrays and
	 * standard java
	 * 
	 * @param o the object to check
	 * @return the attributes and its values in a KJson format
	 * @throws KJsonParserException in case something went wrong
	 */
	private String parsetoKJsonCheckingComplexCases(Object o) throws KJsonParserException {
		if (o == null) {
			return NULL;
		} else if (isPrimitiveType(o.getClass())) {
			return o.toString();
		} else if (o instanceof Collection) {
			return collectionToKJsonString((Collection<?>) o);
		} else if (o.getClass().isArray()) {
			return arrayToKJsonString(unpackArray(o));
		} else if (o instanceof Enum<?>) {
			return enumToKJsonString((Enum<?>) o);
		} else {
			return parseToKJsonString(o);
		}
	}

	/**
	 * Unpacks an array Object that was serialized
	 * 
	 * @param array the object which contains an array
	 * @return the unpacked array object
	 */
	private Object[] unpackArray(Object array) throws KJsonParserException {
		Object[] extractedArray = new Object[Array.getLength(array)];
		for (int i = 0; i < extractedArray.length; i++)
			extractedArray[i] = Array.get(array, i);
		return extractedArray;
	}

	/**
	 * Returns a String with a lists the attributes names and fields. It does not
	 * contain the parsers
	 * 
	 * @param o the Object to parse
	 * @return String with a lists the attributes names and fields
	 * @throws KJsonParserException if it couldn't be parsed
	 */
	private String toKJsonPlainString(Object o) throws KJsonParserException {
		StringBuilder sb = new StringBuilder();
		for (Field field : getFields(o.getClass())) {
			if (!field.isSynthetic() && !isConstant(field)) {
				if (sb.length() != 0) {
					sb.append(SPLITTER_DELIMITER);
				}
				String fieldName = field.getName();
				field.setAccessible(true);
				try {
					Object newObj = getFieldValue(o, field);
					sb.append(fieldName).append(ELEMENT_VALUE_DELIMITER)
							.append(parsetoKJsonCheckingComplexCases(newObj));
				} catch (IllegalArgumentException | IllegalAccessException e) {
					throw new KJsonParserException("Illegal argument or access on " + fieldName, e);
				} finally {
					field.setAccessible(false);
				}
			}
		}
		return sb.toString();
	}

	/**
	 * Retrieves the value of a certain field.
	 * 
	 * Generated to ease mocking issues
	 * 
	 * @param o     Object whose content we want to retrieve
	 * @param field the field we want to retrieve
	 * @return the value of a field
	 * @throws IllegalArgumentException in case the field name is invalid
	 * @throws IllegalAccessException   in case the access to that field is
	 *                                  forbidden
	 */
	public Object getFieldValue(Object o, Field field) throws IllegalArgumentException, IllegalAccessException {
		return field.get(o);
	}

	/**
	 * Recursive method to check also the superclass attributes
	 * 
	 * @param type the class type
	 * @return a list with all the fields of an Object
	 */
	private List<Field> getFields(Class<?> type) {
		List<Field> listFields = new LinkedList<Field>();
		listFields.addAll(Arrays.asList(type.getDeclaredFields()));

		if (type.getSuperclass() != null) {
			listFields.addAll(getFields(type.getSuperclass()));
		}
		return listFields;
	}

	/**
	 * Parses the content of a Collection of elements into a KJson string
	 * 
	 * @param collection a collection of elements
	 * @return the collection of elements parsed to a KJson string
	 * @throws KJsonParserException if there was an error during parsing
	 */
	private String collectionToKJsonString(Collection<?> collection) throws KJsonParserException {
		StringBuilder sb = new StringBuilder();
		sb.append(OPEN_DELIMITER).append(getTypeInterface(collection)).append(ELEMENT_CLASS_DELIMITER);
		Iterator<?> it = collection.iterator();
		if (!it.hasNext()) {
			sb.append(NULL);
		} else {
			while (it.hasNext()) {
				Object o = it.next();
				sb.append(parsetoKJsonCheckingComplexCases(o));
				if (it.hasNext()) {
					sb.append(SPLITTER_COLLECTION_DELIMITER);
				}
			}
		}
		sb.append(CLOSE_DELIMITER);
		return sb.toString();
	}

	/**
	 * Allows to get the interface instead of the class from an object instance
	 * 
	 * @param o the Object instance
	 * @return the interface of the collection instance
	 */
	private Object getTypeInterface(Object o) {
		String type = o.getClass().getSimpleName();
		Class<?>[] interfaces = o.getClass().getInterfaces();
		if (interfaces.length > 0) {
			type = interfaces[0].getSimpleName();
		}
		return type;
	}

	/**
	 * Parses the content of an array of elements into a KJson string
	 * 
	 * @param array an array of elements which should be parsed
	 * @return the array of elements parsed to a KJson string
	 * @throws KJsonParserException if there was an error during parsing
	 */
	private String arrayToKJsonString(Object[] array) throws KJsonParserException {
		StringBuilder sb = new StringBuilder();
		sb.append(OPEN_DELIMITER);
		StringBuilder sbElements = new StringBuilder();
		for (Object element : array) {
			if (sbElements.length() != 0) {
				sbElements.append(SPLITTER_COLLECTION_DELIMITER);
			}
			sbElements.append(parsetoKJsonCheckingComplexCases(element));
		}
		sb.append(sbElements).append(CLOSE_DELIMITER);
		return sb.toString();
	}

	/**
	 * Parses the value of an enumeration instance into a KJson string
	 * 
	 * @param enumObject an enumeration value
	 * @return the enumeration value parsed to a KJson string
	 * @throws KJsonParserException if there was an error during parsing
	 */
	private String enumToKJsonString(Enum<?> enumObject) {
		StringBuilder sb = new StringBuilder(OPEN_DELIMITER);
		sb.append(enumObject.getClass().getSimpleName()).append(ELEMENT_CLASS_DELIMITER).append(enumObject.name())
				.append(CLOSE_DELIMITER);
		return sb.toString();
	}

	/**
	 * Checks if a class is a Java primitive one
	 * 
	 * @param clazz the class to check
	 * @return true if a class is a Java primitive one. Otherwise it returns false
	 */
	public boolean isPrimitiveType(Class<?> clazz) {
		return getPrimitiveTypes().contains(clazz);
	}

	/**
	 * Generates a set containing the primitive classes
	 * 
	 * @return a set with all the classes we consider 'primitive', so their content
	 *         will be transformed directly into a String
	 */
	private Set<Class<?>> getPrimitiveTypes() {
		Set<Class<?>> ret = new HashSet<Class<?>>();
		ret.add(Void.class);
		ret.add(Boolean.class);
		ret.add(Character.class);
		ret.add(Byte.class);
		ret.add(Short.class);
		ret.add(Integer.class);
		ret.add(Long.class);
		ret.add(Float.class);
		ret.add(Double.class);
		ret.add(String.class);
		return ret;
	}

	/**
	 * Detects the constants: meaning the static final attributes
	 * 
	 * @param field a field to check
	 * @return true if it is a constant, otherwise it return false
	 */
	private boolean isConstant(Field field) {
		boolean result = false;
		if (java.lang.reflect.Modifier.isFinal(field.getModifiers())
				|| java.lang.reflect.Modifier.isStatic(field.getModifiers())) {
			result = true;
		}
		return result;
	}

}
