package org.thalion.parser;

/**
 * Controls the KJson parsers exceptions
 * 
 * @author Angeles Broullon Lozano
 * @version 1.1
 *
 */
public class KJsonParserException extends Exception {

	private static final long serialVersionUID = 2232553008907134519L;

	/**
	 * Constructor with parameters
	 * 
	 * @param message the error message
	 * @param cause   the cause of the error
	 */
	public KJsonParserException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructor with parameters
	 * 
	 * @param message the error message
	 */
	public KJsonParserException(String message) {
		super(message);
	}

}
