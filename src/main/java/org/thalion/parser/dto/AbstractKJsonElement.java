package org.thalion.parser.dto;

import java.io.IOException;

/**
 * Abstract implementation for a parseable object
 * 
 * @author Angeles Broullon Lozano
 * @version 1.1
 *
 */
public abstract class AbstractKJsonElement implements IKJsonElement {

	/**
	 * Simple abstract call to the KJson parser
	 * 
	 * @return the object parsed to KJson
	 * @throws IOException if the object couldn't be parsed
	 */
	public String toKJsonString() throws IOException {
		return ParserUtil.parseToKJsonString(this);
	}

}
