package org.thalion.parser.dto;

import java.io.IOException;

/**
 * Interface for a parseable object
 * 
 * @author Angeles Broullon Lozano
 * @version 1.1
 *
 */
public interface IKJsonElement {

	/**
	 * Makes an object parseable to custom KJon
	 * 
	 * @return a String contained the object parsed to KJson format
	 * @throws IOException if the object couldn't be parsed
	 */
	String toKJsonString() throws IOException;

}
