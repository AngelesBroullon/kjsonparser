package org.thalion.parser.dto;

import java.io.IOException;

import org.thalion.parser.KJsonParser;
import org.thalion.parser.KJsonParserException;

/**
 * The parser element, encapsulating the real parser.
 * 
 * This way we may avoid getting the parser as an extra attribute.
 * 
 * @author Angeles Broullon Lozano
 * @version 1.1
 *
 */
public class ParserUtil {

	/**
	 * The KJson parser instance, singleton pattern
	 */
	private static KJsonParser parser;

	/**
	 * Initializes the parser
	 */
	private static void initializeParser() {
		if (parser == null) {
			parser = new KJsonParser();
		}
	}

	/**
	 * Parses an object fields into a KJson String
	 * 
	 * @param o the object to parse
	 * @return the object parsed as a String
	 * @throws IOException in case something went wrong
	 */
	public static String parseToKJsonString(Object o) throws IOException {
		initializeParser();
		try {
			return parser.parseToKJsonString(o);
		} catch (KJsonParserException e) {
			throw new IOException(e);
		}
	}

}
