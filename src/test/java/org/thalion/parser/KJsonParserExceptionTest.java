package org.thalion.parser;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit test for the KJsonParserException
 * @author Angeles Broullon
 *
 */
public class KJsonParserExceptionTest {
	
	private static final String DUMMY_MESSAGE = "There was an error";

	private Throwable dummyCause;
	
	@Test
	public void constructorOneParameterTest() {
		Assert.assertNotNull(new KJsonParserException(DUMMY_MESSAGE));
	}
	
	@Test
	public void constructorTwoParametersTest() {
		Assert.assertNotNull(new KJsonParserException(DUMMY_MESSAGE, dummyCause));
	}

}
