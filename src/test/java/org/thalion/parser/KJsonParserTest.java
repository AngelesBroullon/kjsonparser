package org.thalion.parser;

import java.lang.reflect.Field;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.thalion.parser.dto.AbstractKJsonElement;

@RunWith(MockitoJUnitRunner.class)
public class KJsonParserTest {

	private static final int DUMMY_ID = 1;

	private KJsonParser instance;

	@Mock
	private KJsonParser mock;

	@Before
	public void setUp() {
		instance = new KJsonParser();
	}

	@Test
	public void isPrimitiveType_isValid() {
		Assert.assertTrue(instance.isPrimitiveType(String.class));
	}

	@Test
	public void isPrimitiveType_notValid() {
		Assert.assertFalse(instance.isPrimitiveType(Object.class));
	}

	@Test(expected = KJsonParserException.class)
	public void parseToKJsonString_nullValue() throws KJsonParserException {
		instance.parseToKJsonString(null);
	}

	@Test(expected = KJsonParserException.class)
	public void parseToKJsonString_illegalArgument()
			throws KJsonParserException, IllegalArgumentException, IllegalAccessException {
		Mockito.when(mock.getFieldValue(Matchers.anyObject(), Matchers.any(Field.class)))
				.thenThrow(new IllegalArgumentException());
		Mockito.when(mock.parseToKJsonString(Matchers.anyObject())).thenCallRealMethod();

		DummyRequest request = new DummyRequest();
		request.setId(DUMMY_ID);
		mock.parseToKJsonString(request);
	}

	@Test(expected = KJsonParserException.class)
	public void parseToKJsonString_illegalAccess()
			throws KJsonParserException, IllegalArgumentException, IllegalAccessException {
		Mockito.when(mock.getFieldValue(Matchers.anyObject(), Matchers.any(Field.class)))
				.thenThrow(new IllegalAccessException());
		Mockito.when(mock.parseToKJsonString(Matchers.anyObject())).thenCallRealMethod();

		DummyRequest request = new DummyRequest();
		request.setId(DUMMY_ID);
		mock.parseToKJsonString(request);
	}

	private class DummyRequest extends AbstractKJsonElement {
		private int id;

		@SuppressWarnings("unused")
		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}
	}

}
