package org.thalion.parser.dto;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ org.thalion.parser.dto.KJsonComplexRequestTest.class,
	org.thalion.parser.dto.KJsonNoConstantsAndArrayTest.class, org.thalion.parser.dto.KJsonRecursiveRequestTest.class,
	org.thalion.parser.dto.KJsonSimpleRequestTest.class })
public class AbstractKJsonElementTest {

}
