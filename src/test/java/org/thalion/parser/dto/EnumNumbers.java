package org.thalion.parser.dto;

/**
 * A simple enumeration for testing purposes
 * 
 * @author Angeles Broullon Lozano
 * @version 1.0
 *
 */
public enum EnumNumbers {
	ONE, TWO, THREE;
}
