package org.thalion.parser.dto;

import java.util.List;

import org.thalion.parser.dto.AbstractKJsonElement;

/**
 * Sample for a simple request, containing complex elements elements, such as
 * Collection elements, arrays or custom classes
 * 
 * @author Angeles Broullon Lozano
 * @version 1.0
 *
 */
public class KJsonComplexRequest extends AbstractKJsonElement {

	private List<String> idsList;
	private List<String> namesList;
	private KJsonSimpleRequest innerRequest;
	private int[] table;
	private EnumNumbers number;

	public List<String> getIdsList() {
		return idsList;
	}

	public void setIdsList(List<String> idsList) {
		this.idsList = idsList;
	}

	public List<String> getNamesList() {
		return namesList;
	}

	public void setNamesList(List<String> namesList) {
		this.namesList = namesList;
	}

	public KJsonSimpleRequest getInnerRequest() {
		return innerRequest;
	}

	public void setInnerRequest(KJsonSimpleRequest innerRequest) {
		this.innerRequest = innerRequest;
	}

	public int[] getTable() {
		return table;
	}

	public void setTable(int[] table) {
		this.table = table;
	}

	public EnumNumbers getNumber() {
		return number;
	}

	public void setNumber(EnumNumbers number) {
		this.number = number;
	}

}
