package org.thalion.parser.dto;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for the parser on ComplexRequest, with basic attributes
 * 
 * @author Angeles Broullon Lozano
 * @version 1.0
 *
 */
public class KJsonComplexRequestTest {

	private static final int REQUEST_ID = 1;
	private static final String REQUEST_NAME = "MyRequest";
	private static final String DUMMY_ID = "DummyId";
	private static final String DUMMY_ID_2 = "DummyId_2";
	private static final String DUMMY_NAME = "DummyName";

	private KJsonComplexRequest request;

	@Before
	public void setUp() {
		KJsonSimpleRequest innerRequest = new KJsonSimpleRequest();
		innerRequest.setId(REQUEST_ID);
		innerRequest.setName(REQUEST_NAME);

		List<String> idsList = new LinkedList<String>();
		idsList.add(DUMMY_ID);
		idsList.add(DUMMY_ID_2);

		List<String> namesList = new LinkedList<String>();
		namesList.add(DUMMY_NAME);

		int[] table = { 1, 2 };

		request = new KJsonComplexRequest();
		request.setIdsList(idsList);
		request.setNamesList(namesList);
		request.setInnerRequest(innerRequest);
		request.setTable(table);
		request.setNumber(EnumNumbers.ONE);
	}

	@Test
	public void constructorTest() {
		Assert.assertNotNull(new KJsonComplexRequest());
	}

	@Test
	public void testComplexRequest() throws IOException {
		Assert.assertEquals(getExpectedString(), request.toKJsonString());
	}

	@Test
	public void testComplexRequest_emptyList() throws IOException {
		request.setIdsList(new LinkedList<String>());
		Assert.assertEquals(getExpectedString(), request.toKJsonString());
	}

	public String getExpectedString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[").append(request.getClass().getSimpleName()).append(": idsList = ")
				.append(toCollectionKJsonString(request.getIdsList())).append("; namesList = ")
				.append(toCollectionKJsonString(request.getNamesList())).append("; innerRequest = ")
				.append(getInnerRequestString(request.getInnerRequest())).append("; table = ")
				.append(toTableKJsonString(request.getTable())).append("; number = ")
				.append(toEnumKJsonString(request.getNumber())).append("]");
		return sb.toString();
	}

	private String toCollectionKJsonString(Collection<?> collection) {
		StringBuilder sb = new StringBuilder();
		sb.append("[List: ");
		if (collection == null || collection.isEmpty()) {
			sb.append("null");
		} else {
			Iterator<?> it = collection.iterator();
			while (it.hasNext()) {
				sb.append(it.next());
				if (it.hasNext()) {
					sb.append(", ");
				}
			}
		}
		sb.append("]");
		return sb.toString();
	}

	private String getInnerRequestString(KJsonSimpleRequest request) {
		StringBuilder sb = new StringBuilder();
		sb.append("[").append(request.getClass().getSimpleName()).append(": ").append("id = ").append(request.getId())
				.append("; name = ").append(request.getName()).append("]");
		return sb.toString();
	}

	private String toTableKJsonString(int[] array) {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		StringBuilder sbElements = new StringBuilder();
		for (int element : array) {
			if (sbElements.length() != 0) {
				sbElements.append(", ");
			}
			sbElements.append(element);
		}
		sb.append(sbElements).append("]");
		return sb.toString();
	}

	private String toEnumKJsonString(Enum<?> enumObject) {
		StringBuilder sb = new StringBuilder();
		sb.append("[").append(enumObject.getClass().getSimpleName()).append(": ").append(enumObject.name()).append("]");
		return sb.toString();
	}

}
