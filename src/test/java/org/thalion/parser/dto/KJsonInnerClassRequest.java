package org.thalion.parser.dto;

import org.thalion.parser.dto.AbstractKJsonElement;

/**
 * Sample for an id plus simple request
 * 
 * @author Angeles Broullon Lozano
 * @version 1.0
 *
 */
public class KJsonInnerClassRequest extends AbstractKJsonElement {

	private int id;
	private KJsonSimpleRequest request;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public KJsonSimpleRequest getRequest() {
		return request;
	}

	public void setRequest(KJsonSimpleRequest request) {
		this.request = request;
	}

}
