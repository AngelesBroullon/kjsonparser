package org.thalion.parser.dto;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for the parser on SimpleRequest, with basic attributes
 * 
 * @author Angeles Broullon Lozano
 * @version 1.0
 *
 */
public class KJsonInnerClassRequestTest {

	private static final int REQUEST_ID = 1;
	private static final int INNER_REQUEST_ID = 2;
	private static String INNER_REQUEST_NAME = "MyRequest";

	private KJsonInnerClassRequest request;

	@Before
	public void setUp() {
		KJsonSimpleRequest innerRequest = new KJsonSimpleRequest();
		innerRequest.setId(INNER_REQUEST_ID);
		innerRequest.setName(INNER_REQUEST_NAME);
		request = new KJsonInnerClassRequest();
		request.setId(REQUEST_ID);
		request.setRequest(innerRequest);
	}

	@Test
	public void constructorTest() {
		Assert.assertNotNull(new KJsonInnerClassRequest());
	}

	@Test
	public void testInnerClassRequest() throws IOException {
		Assert.assertEquals(getExpectedString(), request.toKJsonString());
	}

	@Test
	public void testInnerClassRequest_nullInner() throws IOException {
		request.setRequest(null);
		Assert.assertEquals(getExpectedString(), request.toKJsonString());
	}

	private String getExpectedString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[").append(request.getClass().getSimpleName()).append(": ").append("id = ").append(request.getId())
				.append("; request = ").append(getExpectedInnerRequestString(request.getRequest())).append("]");
		return sb.toString();
	}

	private String getExpectedInnerRequestString(KJsonSimpleRequest innerRequest) {
		if (innerRequest != null) {
			StringBuilder sb = new StringBuilder();
			sb.append("[").append(innerRequest.getClass().getSimpleName()).append(": ").append("id = ")
					.append(innerRequest.getId()).append("; name = ").append(innerRequest.getName()).append("]");
			return sb.toString();
		}
		return null;

	}

}
