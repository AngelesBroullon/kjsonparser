package org.thalion.parser.dto;

import org.thalion.parser.dto.AbstractKJsonElement;

/**
 * Sample for a simple request, containing an inheritance
 * 
 * @author Angeles Broullon Lozano
 * @version 1.0
 *
 */
public class KJsonNoConstantsAndArray extends AbstractKJsonElement {

	public static final String CONSTANT_FIELD = "This is a constant";
	public final String FINAL_FIELD = "This is a constant";
	public static String STATIC_FIELD = "This is a constant";

	private int[][] array;

	public int[][] getArray() {
		if (array != null) {
			return array;
		}
		return new int[0][0];
	}

	public void setArray(int[][] newArray) {
		array = new int[0][0];
		if (newArray != null) {
			this.array = newArray.clone();
		}
	}
	
}
