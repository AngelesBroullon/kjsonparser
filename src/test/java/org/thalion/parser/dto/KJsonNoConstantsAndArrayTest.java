package org.thalion.parser.dto;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for the parser on a Request which has constants, and a
 * bidimensional array
 * 
 * @author Angeles Broullon Lozano
 * @version 1.0
 *
 */
public class KJsonNoConstantsAndArrayTest {

	private KJsonNoConstantsAndArray request;

	@Before
	public void setUp() {
		request = new KJsonNoConstantsAndArray();
		int[][] newArray = new int[2][2];
		int value = 1;
		for (int i = 0; i < newArray.length; i++) {
			for (int j = 0; j < newArray[i].length; j++) {
				newArray[i][j] = value;
				value++;
			}
		}
		request.setArray(newArray);
	}

	@Test
	public void constructorTest() {
		Assert.assertNotNull(new KJsonNoConstantsAndArray());
	}

	@Test
	public void testSimpleRequest() throws IOException {
		Assert.assertEquals(getExpectedString(), request.toKJsonString());
	}

	private String getExpectedString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[").append(request.getClass().getSimpleName()).append(": array = ")
				.append(toTableKJsonString(request.getArray())).append("]");
		return sb.toString();
	}

	private String toTableKJsonString(int[][] array) {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		StringBuilder sbElements = new StringBuilder();
		for (int i = 0; i < array.length; i++) {
			int[] subArray = array[i];
			if (sbElements.length() != 0) {
				sbElements.append(", ");
			}
			sbElements.append(toTableKJsonString(subArray));
		}
		sb.append(sbElements).append("]");
		return sb.toString();
	}

	private String toTableKJsonString(int[] array) {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		StringBuilder sbElements = new StringBuilder();
		for (int element : array) {
			if (sbElements.length() != 0) {
				sbElements.append(", ");
			}
			sbElements.append(element);
		}
		sb.append(sbElements).append("]");
		return sb.toString();
	}

}
