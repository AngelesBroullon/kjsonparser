package org.thalion.parser.dto;

/**
 * Sample for a simple request, containing an inheritance
 * 
 * @author Angeles Broullon Lozano
 * @version 1.0
 *
 */
public class KJsonRecursiveRequest extends KJsonSimpleRequest {

	private int number;

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

}
