package org.thalion.parser.dto;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for the parser on RecursiveRequest, with inheritance
 * 
 * @author Angeles Broullon Lozano
 * @version 1.0
 *
 */
public class KJsonRecursiveRequestTest {

	private static final int REQUEST_ID = 1;
	private static String REQUEST_NAME = "MyRequest";

	private KJsonRecursiveRequest request;

	@Before
	public void setUp() {
		request = new KJsonRecursiveRequest();
		request.setId(REQUEST_ID);
		request.setName(REQUEST_NAME);
		request.setNumber(REQUEST_ID);
	}

	@Test
	public void constructorTest() {
		Assert.assertNotNull(new KJsonRecursiveRequest());
	}

	@Test
	public void testSimpleRequest() throws IOException {
		Assert.assertEquals(getExpectedString(), request.toKJsonString());
	}

	private String getExpectedString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[").append(request.getClass().getSimpleName()).append(": number = ").append(request.getNumber())
				.append("; id = ").append(request.getId()).append("; name = ").append(request.getName()).append("]");
		return sb.toString();
	}

}
