package org.thalion.parser.dto;

import org.thalion.parser.dto.AbstractKJsonElement;

/**
 * Sample for a simple request, containing only basic elements
 * 
 * @author Angeles Broullon Lozano
 * @version 1.0
 *
 */
public class KJsonSimpleRequest extends AbstractKJsonElement {

	private int id;
	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
