package org.thalion.parser.dto;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for the parser on SimpleRequest, with basic attributes
 * 
 * @author Angeles Broullon Lozano
 * @version 1.0
 *
 */
public class KJsonSimpleRequestTest {

	private static final int REQUEST_ID = 1;
	private static String REQUEST_NAME = "MyRequest";

	private KJsonSimpleRequest request;

	@Before
	public void setUp() {
		request = new KJsonSimpleRequest();
		request.setId(REQUEST_ID);
		request.setName(REQUEST_NAME);
	}

	@Test
	public void constructorTest() {
		Assert.assertNotNull(new KJsonSimpleRequest());
	}

	@Test
	public void testSimpleRequest() throws IOException {
		Assert.assertEquals(getExpectedString(), request.toKJsonString());
	}

	private String getExpectedString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[").append(request.getClass().getSimpleName()).append(": ").append("id = ").append(request.getId())
				.append("; name = ").append(request.getName()).append("]");
		return sb.toString();
	}

}
