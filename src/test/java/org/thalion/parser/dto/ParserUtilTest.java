package org.thalion.parser.dto;

import java.io.IOException;
import java.lang.reflect.Field;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.thalion.parser.KJsonParser;
import org.thalion.parser.KJsonParserException;

@RunWith(MockitoJUnitRunner.class)
public class ParserUtilTest {

	private static final String DUMMY_STRING = "DUMMY";

	@Mock
	private KJsonParser mock;

	@Before
	public void setUp()
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		Field field = ParserUtil.class.getDeclaredField("parser");
		setValueToPrivateField(ParserUtil.class, field, mock);
	}

	private void setValueToPrivateField(Object object, Field privateField, Object newFieldValue)
			throws IllegalArgumentException, IllegalAccessException, SecurityException {
		privateField.setAccessible(true);
		privateField.set(object, newFieldValue);
		privateField.setAccessible(false);
	}

	@Test
	public void testContructor() {
		Assert.assertNotNull(new ParserUtil());
	}

	@Test
	public void parseToKJsonString_validResult() throws KJsonParserException, IOException {
		Mockito.when(mock.parseToKJsonString(Matchers.anyObject())).thenReturn(DUMMY_STRING);
		Assert.assertEquals(DUMMY_STRING, ParserUtil.parseToKJsonString(DUMMY_STRING));
	}

	@Test(expected = IOException.class)
	public void parseToKJsonString_fails() throws KJsonParserException, IOException {
		Mockito.when(mock.parseToKJsonString(Matchers.anyObject())).thenThrow(new KJsonParserException("Error"));
		ParserUtil.parseToKJsonString(DUMMY_STRING);
	}

}
